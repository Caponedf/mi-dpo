﻿using Cz.Fit.Dpo.HomeworkCBI;
using Cz.Fit.Dpo.HomeworkCBI.Arithmetic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cz.Fit.Dpo.UnitTests.CBI
{
    [TestClass]
	public class ArithmeticExpressionPrinterTest
	{
		private readonly TestExpressionsFactory expressionFactory = new TestExpressionsFactory();

        [TestMethod]
		public virtual void TestInOrderPrintingOfExpression1()
		{
			ArithmeticExpression expression = expressionFactory.CreateExpression1();
			ArithmeticExpressionPrinter printer = new ArithmeticExpressionPrinter(expression);

			Assert.AreEqual("(3-(1+2))", printer.PrintInOrder());
		}

        [TestMethod]
        public virtual void TestInOrderPrintingOfExpression2()
		{
			ArithmeticExpression e = expressionFactory.CreateExpression2();
			ArithmeticExpressionPrinter p = new ArithmeticExpressionPrinter(e);

			Assert.AreEqual("((3-1)+2)", p.PrintInOrder());
		}


        [TestMethod]
        public virtual void TestPostOrderPrintingOfExpression1()
		{
			ArithmeticExpression e = expressionFactory.CreateExpression1();
			ArithmeticExpressionPrinter p = new ArithmeticExpressionPrinter(e);

			Assert.AreEqual("3 1 2 + -", p.PrintPostOrder());
		}

        [TestMethod]
        public virtual void TestPostOrderPrintingOfExpression2()
		{
			ArithmeticExpression e = expressionFactory.CreateExpression2();
			ArithmeticExpressionPrinter p = new ArithmeticExpressionPrinter(e);

			Assert.AreEqual("3 1 - 2 +", p.PrintPostOrder());
		}


	}

}