﻿using Cz.Fit.Dpo.HomeworkCBI;
using Cz.Fit.Dpo.HomeworkCBI.Arithmetic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cz.Fit.Dpo.UnitTests.CBI
{

    [TestClass]
	public class IntegrationTest
	{

        [TestMethod]
		public virtual void TestInOrderPrintingOfRPNExpression()
		{
			ArithmeticExpression e = (new ArithmeticExpressionCreator()).CreateExpressionFromRPN("1 2 + 3 4 + -");
			ArithmeticExpressionPrinter p = new ArithmeticExpressionPrinter(e);

			Assert.AreEqual("((1+2)-(3+4))", p.PrintInOrder());
		}

        [TestMethod]
        public virtual void TestPostOrderPrintingOfRPNExpression()
		{
			ArithmeticExpression e = (new ArithmeticExpressionCreator()).CreateExpressionFromRPN("1 2 + 3 4 + -");
			ArithmeticExpressionPrinter p = new ArithmeticExpressionPrinter(e);

			Assert.AreEqual("1 2 + 3 4 + -", p.PrintPostOrder());
		}
	}

}