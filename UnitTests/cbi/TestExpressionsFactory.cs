﻿using Cz.Fit.Dpo.HomeworkCBI.Arithmetic;

namespace Cz.Fit.Dpo.UnitTests.CBI
{
	/// 
	/// <summary>
	/// @author Ondrej Stuchlik
	/// </summary>
	public class TestExpressionsFactory
	{

		/// <summary>
		/// Creates 3 - (1 + 2)
		/// </summary>
		public virtual ArithmeticExpression CreateExpression1()
		{
			NumericOperand op1 = new NumericOperand(1);
			NumericOperand op2 = new NumericOperand(2);
			NumericOperand op3 = new NumericOperand(3);

			BinaryOperator o2 = new AddOperator(op1, op2);
			BinaryOperator o1 = new SubstractOperator(op3, o2);

			return o1;
		}

		/// <summary>
		/// Creates (3 - 1) + 2
		/// </summary>
		public virtual ArithmeticExpression CreateExpression2()
		{
			NumericOperand op1 = new NumericOperand(1);
			NumericOperand op2 = new NumericOperand(2);
			NumericOperand op3 = new NumericOperand(3);

			BinaryOperator o1 = new SubstractOperator(op3, op1);
			BinaryOperator o2 = new AddOperator(o1, op2);

			return o2;
		}

        public virtual ArithmeticExpression CreateExpression3()
        {
            NumericOperand op1 = new NumericOperand(1);
            NumericOperand op2 = new NumericOperand(2);

            BinaryOperator o2 = new AddOperator(op1, op2);

            return o2;
        }
    }

}