﻿using System;
using Cz.Fit.Dpo.HomeworkCBI;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cz.Fit.Dpo.UnitTests.CBI
{

    [TestClass]
	public class ArithmeticExpressionCreatorTest
	{

		private readonly ArithmeticExpressionCreator aec = new ArithmeticExpressionCreator();

        [TestMethod]
		public virtual void TestSimpleExpressionCreation()
		{
			Assert.AreEqual(1, aec.CreateExpressionFromRPN("1").Evaluate());
		}


        [TestMethod]
        public virtual void TestComplexExpressionCreation()
		{
			Assert.AreEqual(4, aec.CreateExpressionFromRPN("3 1 +").Evaluate());
			Assert.AreEqual(0, aec.CreateExpressionFromRPN("3 1 2 + -").Evaluate());
			Assert.AreEqual(0, aec.CreateExpressionFromRPN("1 2 3 - +").Evaluate());
			Assert.AreEqual(-2, aec.CreateExpressionFromRPN("3 1 + 4 - 2 -").Evaluate());
		}

        [TestMethod]
        [ExpectedException(typeof(ArithmeticExpressionBuilderArgumentException))]

        public virtual void TestInvalidExpressionCreation()
		{
			Assert.AreEqual(1, aec.CreateExpressionFromRPN("1 2 Baf").Evaluate());
		}

		/// <summary>
		/// Empty expression is not valid expression </summary>
        [TestMethod]
        [ExpectedException(typeof(ArithmeticExpressionBuilderArgumentException))]
        public virtual void TestEmptyExpressionCreation()
		{
			Assert.AreEqual(1, aec.CreateExpressionFromRPN("").Evaluate());
		}
	}

}