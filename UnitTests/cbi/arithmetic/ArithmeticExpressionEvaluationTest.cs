﻿using Cz.Fit.Dpo.HomeworkCBI;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cz.Fit.Dpo.UnitTests.CBI.Arithmetic
{

    [TestClass]
	public class ArithmeticExpressionEvaluationTest
	{
		private readonly TestExpressionsFactory expressionFactory = new TestExpressionsFactory();

        [TestMethod]
		public virtual void TestExpression1Evaluation()
		{
			Assert.AreEqual(0, expressionFactory.CreateExpression1().Evaluate());
		}

        [TestMethod]
        public virtual void TestExpression2Evaluation()
		{
			Assert.AreEqual(4, expressionFactory.CreateExpression2().Evaluate());
		}


	}

}