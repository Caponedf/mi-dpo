﻿using Cz.Fit.Dpo.HomeworkCBI.Arithmetic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cz.Fit.Dpo.UnitTests.CBI.Arithmetic
{

    [TestClass]
	public class AddOperatorTest
	{

		private readonly NumericOperand o1 = new NumericOperand(1);
		private readonly NumericOperand o2 = new NumericOperand(2);

        [TestMethod]
		public virtual void TestGetFirstOperand()
		{
			AddOperator o = new AddOperator(o1, o2);
			Assert.AreEqual(o1, o.FirstOperand);
		}

        [TestMethod]
        public virtual void TestGetSeconTdOperand()
		{
			AddOperator o = new AddOperator(o1, o2);
			Assert.AreEqual(o2, o.SecondOperand);
		}
	}

}