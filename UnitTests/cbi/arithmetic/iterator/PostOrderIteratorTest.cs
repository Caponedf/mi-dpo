﻿using System;
using Cz.Fit.Dpo.HomeworkCBI.Arithmetic;
using Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Elements;
using Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Iterator;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cz.Fit.Dpo.UnitTests.CBI.Arithmetic.iterator
{


    [TestClass]
	public class PostOrderIteratorTest
	{

		private readonly TestExpressionsFactory expressionFactory = new TestExpressionsFactory();

        [TestMethod]
		public virtual void TestIteration()
		{
			ArithmeticExpression e = expressionFactory.CreateExpression1();
			IIterator<ExpressionElement> it = e.GetPostOrderIterator();
			Assert.IsNotNull(it);

			Assert.AreEqual("3", it.Next().StringValue());
			Assert.AreEqual("1", it.Next().StringValue());
			Assert.AreEqual("2", it.Next().StringValue());
			Assert.AreEqual("+", it.Next().StringValue());
			Assert.AreEqual("-", it.Next().StringValue());
			Assert.IsFalse(it.HasNext);
		}

        [TestMethod]
        public void TestIteration2()
        {
            Assert.IsTrue(true);
        }

        [TestMethod]
        [ExpectedException(typeof(NotSupportedException))]
        public virtual void TestRemoveOperationUnsupported()
		{
			ArithmeticExpression e = expressionFactory.CreateExpression1();
			IIterator<ExpressionElement> it = e.GetPostOrderIterator();
            Assert.IsNotNull(it);
			it.Remove();
		}
	}
}