﻿using System.Linq;
using Cz.Fit.Dpo.HomeworkCBI.Arithmetic;

namespace Cz.Fit.Dpo.HomeworkCBI
{


	public class ArithmeticExpressionCreator
	{
        /// <summary>
        /// Creates any expression from the RPN input. 
        /// </summary>
        /// <seealso cref="http://en.wikipedia.org/wiki/Reverse_Polish_notation"></seealso>
        /// <param name="input"> in Reverse Polish Notation </param>
        /// <returns> <seealso cref="ArithmeticExpression"/> equivalent of the RPN input. </returns>
        public ArithmeticExpression CreateExpressionFromRPN(string input)
        {
            var builder = new ArithmeticExpressionBuilder();

            foreach (var expression in input.Split(' '))
            {
                if (expression.Any() && expression.All(char.IsNumber))
                {
                    builder.BuildNumericOperand(expression);
                    continue;
                }

               

                switch (expression)
                {
                    case "+":
                        builder.BuildAddOperator();
                        break;
                    case "-":
                        builder.BuildSubstractOperator();
                        break;
                    default:
                            throw new ArithmeticExpressionBuilderArgumentException();
                }
            }

            return builder.GetArithmeticExpression();
        }

	   
	}

}