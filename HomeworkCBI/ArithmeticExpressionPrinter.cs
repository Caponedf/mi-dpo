﻿using Cz.Fit.Dpo.HomeworkCBI.Arithmetic;

namespace Cz.Fit.Dpo.HomeworkCBI
{
	/// <summary>
	/// Printer for <seealso cref="ArithmeticExpression"/>s. It can print inOrder notation (3 + 1) or postOrder
	/// notation (3 1 +).
	/// 
	/// PostOrder is RPN (Reverse Polish Notation) in fact. See wiki for more information.
	/// 
	/// </summary>
	public class ArithmeticExpressionPrinter
	{
		private readonly ArithmeticExpression expression;

		public ArithmeticExpressionPrinter(ArithmeticExpression expression)
		{
			this.expression = expression;
		}

		/// <summary>
		/// Print an expression in classical notation, e.g. (3+1).     
		/// The "(" and ")" is used to preserve priorities. </summary>
		/// <returns> String in classical "inOrder" format. </returns>
		public virtual string PrintInOrder()
		{
		    var result = string.Empty;
		    var iterator = expression.GetInOrderIterator();
            while (iterator.HasNext)
                result += iterator.Next().StringValue();

		    return result.Trim();
		}

		/// <summary>
		/// Print an expression in RPN notation, e.g. 3 1 +.     
		/// Please note, the "(" and ")" are no longer necessary, because RPN does not need them. </summary>
		/// <returns> string in "postOrder" (RPN) format. </returns>
		public virtual string PrintPostOrder()
		{
            var result = string.Empty;
		    var iterator = expression.GetPostOrderIterator();
            while (iterator.HasNext)
                result += iterator.Next().StringValue() + " ";

            return result.Trim();
        }

	}

}