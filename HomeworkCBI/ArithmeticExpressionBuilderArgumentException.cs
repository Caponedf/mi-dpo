﻿using System;

namespace Cz.Fit.Dpo.HomeworkCBI
{
    public class ArithmeticExpressionBuilderArgumentException : ArgumentException
    {
        public override string Message =>
            "Expression has not sufficient number of argument before this operator.";
    }
}