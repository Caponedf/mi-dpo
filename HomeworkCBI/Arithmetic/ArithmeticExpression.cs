﻿using Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Elements;
using Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Iterator;

namespace Cz.Fit.Dpo.HomeworkCBI.Arithmetic
{


	public abstract class ArithmeticExpression
	{

		public abstract int? Evaluate();

	    public abstract IIterator<ExpressionElement> GetInOrderIterator();

	    public abstract IIterator<ExpressionElement> GetPostOrderIterator();

        public override bool Equals(object obj)
        {
            var numericOperand = obj as ArithmeticExpression;

            if (numericOperand == null)
                return false;

            return Equals(numericOperand);


        }

        protected bool Equals(NumericOperand other)
        {
            return Evaluate() == other.Evaluate();
        }

        public override int GetHashCode()
        {
            return Evaluate().GetHashCode();
        }
    }

}