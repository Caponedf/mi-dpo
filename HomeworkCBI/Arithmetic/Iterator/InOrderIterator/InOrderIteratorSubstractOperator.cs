using Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Elements;

namespace Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Iterator.InOrderIterator
{
    public class InOrderIteratorSubstractOperator : InOrderIteratorBinaryOperator<SubstractOperation>
    {
        public InOrderIteratorSubstractOperator(SubstractOperator operand) : base(operand)
        {
        }
    }
}