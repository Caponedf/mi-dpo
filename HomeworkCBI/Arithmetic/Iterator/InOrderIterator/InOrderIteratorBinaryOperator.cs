using System;
using Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Elements;

namespace Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Iterator.InOrderIterator
{
    public abstract class InOrderIteratorBinaryOperator<TOperator> : InOrderIterator 
        where TOperator : ExpressionElement, new()
    {
        private InIteratorAddOperatorState operatorState;
        private readonly IIterator<ExpressionElement> firstInOrderIterator;
        private readonly IIterator<ExpressionElement> secondInOrderIterator;

        private enum InIteratorAddOperatorState
        {
            OpenBracket,
            First,
            Operand,
            Second,
            ClosingBracket,
            End
        }

        protected InOrderIteratorBinaryOperator(BinaryOperator operand)
        {
            firstInOrderIterator = operand.FirstOperand.GetInOrderIterator();
            secondInOrderIterator = operand.SecondOperand.GetInOrderIterator();

        }

        public override ExpressionElement Next()
        {
            switch (operatorState)
            {
                case InIteratorAddOperatorState.OpenBracket:
                    operatorState = InIteratorAddOperatorState.First;
                    return new OpenBracketOperation();
                case InIteratorAddOperatorState.First:
                    var res = firstInOrderIterator.Next();
                    if (!firstInOrderIterator.HasNext)
                        operatorState = InIteratorAddOperatorState.Operand;
                    return res;
                case InIteratorAddOperatorState.Operand:
                    operatorState = InIteratorAddOperatorState.Second;
                    return new TOperator();
                case InIteratorAddOperatorState.Second:
                    res = secondInOrderIterator.Next();
                    if (!secondInOrderIterator.HasNext)
                        operatorState = InIteratorAddOperatorState.ClosingBracket;
                    return res;
                case InIteratorAddOperatorState.ClosingBracket:
                    operatorState = InIteratorAddOperatorState.End;
                    return new CloseBracketOperation();
                default:
                    throw new Exception("Cannot iterate this binary operator. Is at the end.");
            }
        }

        public override bool HasNext => operatorState != InIteratorAddOperatorState.End;
    }
}