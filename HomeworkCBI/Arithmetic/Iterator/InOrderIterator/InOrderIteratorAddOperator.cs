using Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Elements;

namespace Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Iterator.InOrderIterator
{
    public class InOrderIteratorAddOperator : InOrderIteratorBinaryOperator<AddOperation>
    {
        public InOrderIteratorAddOperator(AddOperator operand) : base(operand)
        {
        }
    }
}