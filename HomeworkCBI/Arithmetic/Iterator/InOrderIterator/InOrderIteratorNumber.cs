﻿using Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Elements;

namespace Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Iterator.InOrderIterator
{
    public class InOrderIteratorNumber : InOrderIterator
    {
        private readonly NumericOperand number;

        public override bool HasNext => false;

        public override ExpressionElement Next()
        {
            return new Number(number.Evaluate());
        }


        public InOrderIteratorNumber(NumericOperand number)
        {
            this.number = number;
        }
    }
}