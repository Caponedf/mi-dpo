﻿using Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Elements;

namespace Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Iterator
{
    public interface IIterator<out T>
    {
        bool HasNext { get; }

        T Next();

        void Remove();
    }
}