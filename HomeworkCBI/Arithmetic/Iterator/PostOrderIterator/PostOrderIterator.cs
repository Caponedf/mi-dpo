﻿using System;
using Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Elements;

namespace Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Iterator.PostOrderIterator
{


	public abstract class PostOrderIterator : IIterator<ExpressionElement>
	{
	    public abstract bool HasNext { get; }
	    

	    public abstract ExpressionElement Next();

	    public virtual void Remove()
	    {
            throw new NotSupportedException("Remove operation is not supported by this iterator");
        }
	}
}