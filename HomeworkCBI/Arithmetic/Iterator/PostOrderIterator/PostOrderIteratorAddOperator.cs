using Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Elements;

namespace Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Iterator.PostOrderIterator
{
    public class PostOrderIteratorAddOperator : PostOrderIteratorBinaryOperator<AddOperation>
    {
        public PostOrderIteratorAddOperator(AddOperator operand) : base(operand)
        {
        }
    }
}