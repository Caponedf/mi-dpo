using Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Elements;

namespace Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Iterator.PostOrderIterator
{
    public class PostOrderIteratorSubstractOperator : PostOrderIteratorBinaryOperator<SubstractOperation>
    {
        public PostOrderIteratorSubstractOperator(SubstractOperator operand) : base(operand)
        {
        }
    }
}