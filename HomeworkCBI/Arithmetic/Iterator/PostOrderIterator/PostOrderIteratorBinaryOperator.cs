using System;
using Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Elements;

namespace Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Iterator.PostOrderIterator
{
    public abstract class PostOrderIteratorBinaryOperator<TOperator> : PostOrderIterator
        where TOperator : ExpressionElement, new()
    {
        private PostOrderBinaryOperatorState operatorState;
        private readonly IIterator<ExpressionElement> firstInOrderIterator;
        private readonly IIterator<ExpressionElement> secondInOrderIterator;

        private enum PostOrderBinaryOperatorState
        {
            First,
            Second,
            Operand,
            End
        }

        protected PostOrderIteratorBinaryOperator(BinaryOperator operand)
        {
            firstInOrderIterator = operand.FirstOperand.GetPostOrderIterator();
            secondInOrderIterator = operand.SecondOperand.GetPostOrderIterator();
        }

        public override ExpressionElement Next()
        {
            switch (operatorState)
            {
                
                case PostOrderBinaryOperatorState.First:
                    var res = firstInOrderIterator.Next();
                    if (!firstInOrderIterator.HasNext)
                        operatorState = PostOrderBinaryOperatorState.Second;
                    return res;
                
                case PostOrderBinaryOperatorState.Second:
                    res = secondInOrderIterator.Next();
                    if (!secondInOrderIterator.HasNext)
                        operatorState = PostOrderBinaryOperatorState.Operand;
                    return res;
                case PostOrderBinaryOperatorState.Operand:
                    operatorState = PostOrderBinaryOperatorState.End;
                    return new TOperator();
                default:
                    throw new Exception("Cannot iterate this binary operator. Is at the end.");
            }
        }

        public override bool HasNext => operatorState != PostOrderBinaryOperatorState.End;
    }
}