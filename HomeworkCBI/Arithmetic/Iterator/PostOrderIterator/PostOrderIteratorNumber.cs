using Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Elements;

namespace Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Iterator.PostOrderIterator
{
    public class PostOrderIteratorNumber : PostOrderIterator
    {
        private readonly NumericOperand number;

        public override bool HasNext => false;

        public override ExpressionElement Next()
        {
            return new Number(number.Evaluate());
        }


        public PostOrderIteratorNumber(NumericOperand number)
        {
            this.number = number;
        }
    }
}