﻿namespace Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Elements
{

	public class Number : ExpressionElement
	{

		private int? value;

		public Number(int? value)
		{
			this.value = value;
		}

		public override string StringValue()
		{
			return value.ToString();
		}
	}

}