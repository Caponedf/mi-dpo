﻿
namespace Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Elements
{

	public class AddOperation : ExpressionElement
	{

        public override string StringValue()
        {
			return "+";
		}
	}

}