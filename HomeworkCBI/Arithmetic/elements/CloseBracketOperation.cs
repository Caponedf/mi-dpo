﻿namespace Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Elements
{

	public class CloseBracketOperation : ExpressionElement
	{

		public override string StringValue()
		{
			return ")";
		}
	}

}