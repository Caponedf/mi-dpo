﻿namespace Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Elements
{

	public class OpenBracketOperation : ExpressionElement
	{

        public override string StringValue()
        {
			return "(";
		}
	}

}