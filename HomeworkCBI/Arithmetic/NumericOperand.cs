﻿using Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Elements;
using Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Iterator;
using Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Iterator.InOrderIterator;
using Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Iterator.PostOrderIterator;

namespace Cz.Fit.Dpo.HomeworkCBI.Arithmetic
{


    /// <summary>
	/// Represents number in the arithmetic expression
	/// </summary>
	public class NumericOperand : ArithmeticExpression
	{
		private readonly int? value;

		public NumericOperand(int? value)
		{
			this.value = value;
		}

		public override int? Evaluate()
		{
		    return value;
		}

        public override IIterator<ExpressionElement> GetInOrderIterator()
        {
            return new InOrderIteratorNumber(this);
        }

        public override IIterator<ExpressionElement> GetPostOrderIterator()
        {
            return new PostOrderIteratorNumber(this);
        }


    }

}