﻿using System;

namespace Cz.Fit.Dpo.HomeworkCBI.Arithmetic
{
	/// <summary>
	/// Represents the Binary operations like + - etc.
	/// </summary>
	public abstract class BinaryOperator : ArithmeticExpression
	{
	    protected BinaryOperator(ArithmeticExpression firstOperand, ArithmeticExpression secondOperand)
		{
			FirstOperand = firstOperand;
			SecondOperand = secondOperand;
		}

      
        public virtual ArithmeticExpression FirstOperand { get; }

	    public virtual ArithmeticExpression SecondOperand { get; }
	}

}