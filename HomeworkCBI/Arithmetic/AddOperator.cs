﻿using Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Elements;
using Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Iterator;
using Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Iterator.InOrderIterator;
using Cz.Fit.Dpo.HomeworkCBI.Arithmetic.Iterator.PostOrderIterator;

namespace Cz.Fit.Dpo.HomeworkCBI.Arithmetic
{


	/// <summary>
	/// Represents + operation
	/// </summary>
	public class AddOperator : BinaryOperator
	{

        public AddOperator(ArithmeticExpression firstOperand, ArithmeticExpression secondOperand) : base(firstOperand, secondOperand)
	    {

        }

        public override int? Evaluate()
        {
            return FirstOperand.Evaluate() + SecondOperand.Evaluate();
        }

	    public override IIterator<ExpressionElement> GetInOrderIterator()
	    {
	        return new InOrderIteratorAddOperator(this);
	    }

	    public override IIterator<ExpressionElement> GetPostOrderIterator()
	    {
	        return new PostOrderIteratorAddOperator(this);
        }

	}

}