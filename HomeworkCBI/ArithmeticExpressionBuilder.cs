﻿using System;
using System.Collections.Generic;
using Cz.Fit.Dpo.HomeworkCBI.Arithmetic;

namespace Cz.Fit.Dpo.HomeworkCBI
{

    public class ArithmeticExpressionBuilder
    {
        private readonly Stack<ArithmeticExpression> stack = new Stack<ArithmeticExpression>();

        public ArithmeticExpression GetArithmeticExpression()
        {
            return stack.Pop();
        }

        public void BuildNumericOperand(string element)
        {
            int number;
            if (!int.TryParse(element, out number))
                throw new FormatException("Expression is not in correct format");

            stack.Push(new NumericOperand(number));
        }

        public void BuildAddOperator()
        {
            if (stack.Count < 2)
                throw new ArithmeticExpressionBuilderArgumentException();


            var secondOperand = stack.Pop();
            var firstOperand = stack.Pop();

            var result = new AddOperator(firstOperand, secondOperand);
            stack.Push(result);
        }

        public void BuildSubstractOperator()
        {
            if (stack.Count < 2)
                throw new ArithmeticExpressionBuilderArgumentException();

            var secondOperand = stack.Pop();
            var firstOperand = stack.Pop();

            var result = new SubstractOperator(firstOperand, secondOperand);
            stack.Push(result);
        }
    }

  
   
}
