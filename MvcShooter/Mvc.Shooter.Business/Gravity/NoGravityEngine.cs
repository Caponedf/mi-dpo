using Cz.Cvut.Mvc.Shooter.Business.Objects;

namespace Cz.Cvut.Mvc.Shooter.Business.Gravity
{
    public class NoGravityEngine: IGravityEngine
    {
        public string Description => "No gravity";

        public void GravityForMissile(Missile missile)
        {
            
        }

    }
}