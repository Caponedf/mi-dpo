﻿using Cz.Cvut.Mvc.Shooter.Business.Objects;

namespace Cz.Cvut.Mvc.Shooter.Business.Gravity
{
    public interface IGravityEngine
    {
        void GravityForMissile(Missile missile);

        string Description { get; }
    }
}