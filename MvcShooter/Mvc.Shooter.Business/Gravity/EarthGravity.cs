using Cz.Cvut.Mvc.Shooter.Business.Objects;

namespace Cz.Cvut.Mvc.Shooter.Business.Gravity
{
    public class EarthGravity : IGravityEngine
    {
        public string Description => "Earth gravity";

        public double GravityForce 
        {
            get;
            set;
        }

        public EarthGravity(double gravityForce)
        {
            GravityForce = gravityForce;
        }

        public void GravityForMissile(Missile missile)
        {
            missile.GravitySpeed -= GravityForce/200;
        }

    }
}