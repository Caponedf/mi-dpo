﻿using Cz.Cvut.Mvc.Shooter.Business.Objects;

namespace Cz.Cvut.Mvc.Shooter.Business.Atmosfere
{
    public class EarthAtmosfere : IAtmosfere
    {
        double AirPressure { get; set; }

        public EarthAtmosfere(double airPressure)
        {
            AirPressure = airPressure;
        }

        public string Description => "Earth";

        public void AtmosfereForMissile(Missile missile)
        {
            if (missile.Force > 0)
                missile.Force -= AirPressure/10000;
        }
    }
}