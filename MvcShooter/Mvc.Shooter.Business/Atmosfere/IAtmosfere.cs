﻿using Cz.Cvut.Mvc.Shooter.Business.Objects;

namespace Cz.Cvut.Mvc.Shooter.Business.Atmosfere
{
    public interface IAtmosfere
    {
        string Description { get; }

        void AtmosfereForMissile(Missile missile);
    }
}
