﻿namespace Cz.Cvut.Mvc.Shooter.Business.Graphics
{
    public interface IRenderRequest
    {
        double ScreenWidth { get; }
        double ScreenHeight { get; }
    }
}