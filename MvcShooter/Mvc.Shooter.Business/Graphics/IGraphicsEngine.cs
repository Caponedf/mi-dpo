﻿using Cz.Cvut.Mvc.Shooter.Business.Objects;

namespace Cz.Cvut.Mvc.Shooter.Business.Graphics
{
    public interface IGraphicsEngine
    {
        void DrawCannon(object drawingContext, Cannon cannon);

        void DrawMissile(object drawingContext, Missile missile);

        void DrawEnemy(object drawingContext, Enemy enemy);

        void DrawExplosion(object drawingContext, Explosion explosion);
    }
}
