﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Input;
using Cz.Cvut.Mvc.Shooter.Business.Atmosfere;
using Cz.Cvut.Mvc.Shooter.Business.GameLevels;
using Cz.Cvut.Mvc.Shooter.Business.Gravity;
using Cz.Cvut.Mvc.Shooter.Business.Memento;
using Cz.Cvut.Mvc.Shooter.Business.Objects;
using Prism.Commands;

namespace Cz.Cvut.Mvc.Shooter.Business.ViewModel
{
    public enum GameStatus
    {
        NoGame,
        Running,
        Fail,
        Victory
    }

    public class MainViewModel: ViewModelBase, IMementoViewModel
    {
        private readonly IDispatcherService dispatcherService;
        private Timer timer;
        private IGravityEngine selectedGravityEngine;
        private IAtmosfere selectedAtmosfere;
        private int missilesLeft;
        private IGameLevel selectedGameLevel;
        private GameStatus gameStatus;

        public object Mutex { get; }

        public double ScreenWidth { get; set; }
        public double ScreenHeight { get; set; }

        public KeyPadViewModel KeyPadViewModel { get; set; }

        public ICareTaker CareTaker { get; set; }

        /// <summary>
        /// When game canvas need to by redraw. 
        /// </summary>
        public event EventHandler<EventArgs> InvalidateRequested;

        public List<IGravityEngine> GravityEngines { get; set; }
        public List<IAtmosfere> Atmosferes { get; set; }


        public IGravityEngine SelectedGravityEngine
        {
            get { return selectedGravityEngine; }
            set
            {
                selectedGravityEngine = value;
                OnPropertyChanged();
            }
        }


        public IAtmosfere SelectedAtmosfere
        {
            get { return selectedAtmosfere; }
            set
            {
                selectedAtmosfere = value;
                OnPropertyChanged();
            }
        }

        public List<IGameLevel> GameLevels { get; set; }

        public IGameLevel SelectedGameLevel
        {
            get { return selectedGameLevel; }
            set
            {
                selectedGameLevel = value;
                OnPropertyChanged();
            }
        }

        public IQueryable<GameObject> GameObjects
        {
            get
            {
                return
                    Missiles.Cast<GameObject>()
                            .Union(Enemies)
                            .Union(Explosions)
                            .Union(new List<GameObject> { Cannon })
                            .AsQueryable();
            }
        }

        public Cannon Cannon { get; set; }

        public List<Missile> Missiles { get; set; }

        public List<Enemy> Enemies { get; set; }

        public List<Explosion> Explosions { get; set; }

        public int MissilesLeft
        {
            get { return missilesLeft; }
            set
            {
                missilesLeft = value;
                OnPropertyChanged();
            }
        }

        public GameStatus GameStatus
        {
            get { return gameStatus; }
            set
            {
                gameStatus = value;
                OnPropertyChanged();
            }
        }

        public ICommand NewGameCommand { get; private set; }

        public MainViewModel(IDispatcherService dispatcherService)
        {
            this.dispatcherService = dispatcherService;

            Mutex = new object();
            KeyPadViewModel = new KeyPadViewModel();


            Missiles = new List<Missile>();
            Enemies = new List<Enemy>();
            Explosions = new List<Explosion>();

            GravityEngines = new List<IGravityEngine>
            {
                new NoGravityEngine(),
                new EarthGravity(10)
            };

            SelectedGravityEngine = GravityEngines[1];

            Atmosferes = new List<IAtmosfere>
            {
                new Vakuum(),
                new EarthAtmosfere(1000)
            };
            SelectedAtmosfere = Atmosferes[1];

            GameLevels = new List<IGameLevel>
            {
                new EasyLevel(),
                new MediumLevel(),
                new HardLevel(),
                new NightmareLevel()
            };

            SelectedGameLevel = GameLevels.First();

            NewGameCommand = new DelegateCommand(NewGame);

            Cannon = new Cannon(ScreenHeight);


            CareTaker = new CareTaker(this);

        }

        public void SwitchCannonMode()
        {
            Cannon.CannonMode = Cannon.CannonMode == CannonMode.SingleShooting
                ? CannonMode.DoubleShooting
                : CannonMode.SingleShooting;
        }

        private void NewGame()
        {
            lock (Mutex)
            {
                CareTaker.StoreMemento();

                GameStatus = GameStatus.Running;

                Missiles.Clear();
                Enemies.Clear();
                Explosions.Clear();

                MissilesLeft = SelectedGameLevel.Missiles;

                Cannon = new Cannon(ScreenHeight);

                SelectedGameLevel.NewGame(this);
            }
        }

        private void InvestigateItemCollisions()
        {
            var itemsToRemove = new List<Enemy>();
            var explosions = new List<Explosion>();

            foreach (var obj in Missiles)
            {
                var collidingObj =
                    Enemies.Except(itemsToRemove).FirstOrDefault(f => f.CollidesWith(obj));

                if (collidingObj == null)
                    continue;

                itemsToRemove.Add(collidingObj);
                explosions.Add(new Explosion(collidingObj.Left, collidingObj.Top));
            }

            itemsToRemove.ForEach(f => Enemies.Remove(f));
            Explosions.AddRange(explosions);
        }

        private void OnTimerCallback(object state)
        {
            lock (Mutex)
            {
                AdjustCannonAngle();

                FireCannon();

                AdjustCannonForce();


                foreach(var gameObj in GameObjects)
                    gameObj.GameTimer(SelectedGravityEngine, SelectedAtmosfere);

                InvestigateItemCollisions();

                Missiles.Where(w => w.IsExpired).ToList().ForEach(f => Missiles.Remove(f));
                Explosions.Where(w => w.IsExpired).ToList().ForEach(f => Explosions.Remove(f));

                if (MissilesLeft == 0 && !Missiles.Any()  && Enemies.Any())
                    GameStatus = GameStatus.Fail;
                if (GameStatus != GameStatus.NoGame && !Enemies.Any())
                    GameStatus = GameStatus.Victory;

                OnInvalidateRequested();
            }
        }

        private void AdjustCannonForce()
        {
            Cannon.AdjustForce(KeyPadViewModel.IsSpacebarPressed && MissilesLeft > 0);
        }

        private void FireCannon()
        {
            if (!KeyPadViewModel.IsSpacebarPressed && Cannon.IsReadyToFire && MissilesLeft > 0)
            {
                dispatcherService.BeginInvoke(() =>
                {
                    CareTaker.StoreMemento();
                }).Wait();

                Missiles.AddRange(Cannon.Fire());
                MissilesLeft--;
            }
        }

        private void AdjustCannonAngle()
        {
            var cannonAccDir = KeyPadViewModel.IsUpArrowPressed ? AccDirection.Increase : AccDirection.Iddle;
            cannonAccDir = KeyPadViewModel.IsDownArrowPressed ? AccDirection.Decrease : cannonAccDir;


            Cannon?.AdjustAngle(cannonAccDir);
        }

        public void OnViewLoaded()
        {
            Cannon = new Cannon(ScreenHeight);
            timer = new Timer(OnTimerCallback, null, 0, 10);

        }



        protected virtual void OnInvalidateRequested()
        {
            InvalidateRequested?.Invoke(this, EventArgs.Empty);
        }

       

        public IMemento CreateMemento()
        {
            var memento = new MainViewModelMemento
            {
                Cannon = Cannon,
                Missiles = Missiles.ToList(),
                Enemies = Enemies.ToList(),
                MissilesLeft = MissilesLeft,
                GameStatus = GameStatus,
            };

            return memento;
        }


        public void SetMemento(IMemento memento)
        {
            var mainViewModelMemento = memento as MainViewModelMemento;
            if (mainViewModelMemento == null)
                throw new Exception("Memento must be type of MainViewModelMemento");

            Cannon = mainViewModelMemento.Cannon;
            Enemies = mainViewModelMemento.Enemies;
            Missiles = mainViewModelMemento.Missiles;
            MissilesLeft = mainViewModelMemento.MissilesLeft;
            GameStatus = mainViewModelMemento.GameStatus;
        }

        private class MainViewModelMemento : IMemento
        {
            public Cannon Cannon { get; set; }

            public List<Missile> Missiles { get; set; }

            public List<Enemy> Enemies { get; set; }

            public int MissilesLeft { get; set; }

            public GameStatus GameStatus { get; set; }
        }
    }
}
