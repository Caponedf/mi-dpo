using System.ComponentModel;
using System.Runtime.CompilerServices;
using Cz.Cvut.Mvc.Shooter.Business.Annotations;

namespace Cz.Cvut.Mvc.Shooter.Business.ViewModel
{
    public abstract class ViewModelBase: INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}