namespace Cz.Cvut.Mvc.Shooter.Business.ViewModel
{
    public class KeyPadViewModel
    {
        public bool IsUpArrowPressed { get; set; }

        public bool IsDownArrowPressed { get; set; }

        public bool IsSpacebarPressed { get; set; }
    }
}