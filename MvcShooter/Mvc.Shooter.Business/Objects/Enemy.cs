using System;
using Cz.Cvut.Mvc.Shooter.Business.Graphics;

namespace Cz.Cvut.Mvc.Shooter.Business.Objects
{
    public class Enemy : GameObject
    {
        protected readonly Random random;

        public Enemy(Random random)
        {
            this.random = random;
            Height = 30;
            Width = 30;
        }

        public override void Redraw(IGraphicsEngine graphicsEngine, IRenderRequest drawingContext)
        {
            graphicsEngine.DrawEnemy(drawingContext, this);
        }
    }
}