using Cz.Cvut.Mvc.Shooter.Business.Atmosfere;
using Cz.Cvut.Mvc.Shooter.Business.Graphics;
using Cz.Cvut.Mvc.Shooter.Business.Gravity;

namespace Cz.Cvut.Mvc.Shooter.Business.Objects
{
    public class Explosion : GameObject
    {

        private int counter = 100;

        public Explosion(double left, double top)
        {
            Width = 30;
            Height = 30;

            Left = left;
            Top = top;
        }

        public override void Redraw(IGraphicsEngine graphicsEngine, IRenderRequest drawingContext)
        {
            graphicsEngine.DrawExplosion(drawingContext, this);
        }

        public override void GameTimer(IGravityEngine gravityEngine, IAtmosfere atmosfere)
        {
            base.GameTimer(gravityEngine, atmosfere);

            counter--;

            IsExpired = counter < 0;
        }
    }
}