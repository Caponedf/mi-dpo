﻿using System;
using Cz.Cvut.Mvc.Shooter.Business.Atmosfere;
using Cz.Cvut.Mvc.Shooter.Business.Graphics;
using Cz.Cvut.Mvc.Shooter.Business.Gravity;

namespace Cz.Cvut.Mvc.Shooter.Business.Objects
{
    public class Missile : GameObject
    {

        public double Angle { get; set; }

        public double Force { get; set; }

        public double GravitySpeed { get; set; }

        public Missile(double angle, double force, double left, double top)
        {
            Angle = angle;
            Force = force;

            Width = 30;
            Height = 30;

            Left = left;
            Top = top - Height/2;
        }

        public override void Redraw(IGraphicsEngine graphicsEngine, IRenderRequest drawingContext)
        {
            if (Left < 0 ||Left > drawingContext.ScreenWidth || Top > drawingContext.ScreenHeight)
                IsExpired = true;

            graphicsEngine.DrawMissile(drawingContext, this);
        }

        public override void GameTimer(IGravityEngine gravityEngine, IAtmosfere atmosfere)
        {
            base.GameTimer(gravityEngine, atmosfere);

            gravityEngine.GravityForMissile(this);
            atmosfere.AtmosfereForMissile(this);

            var radians = (Math.PI / 180) * Angle;
            var missileStepLeft = Math.Cos(radians);
            var missileStepTop = Math.Sin(radians) ;

            Left += (Force/10)*missileStepLeft;
            Top += (Force / 10) * missileStepTop;
            Top -= GravitySpeed;


        }
    }
}