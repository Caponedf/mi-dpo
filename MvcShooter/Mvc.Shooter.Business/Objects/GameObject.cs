using Cz.Cvut.Mvc.Shooter.Business.Atmosfere;
using Cz.Cvut.Mvc.Shooter.Business.Graphics;
using Cz.Cvut.Mvc.Shooter.Business.Gravity;

namespace Cz.Cvut.Mvc.Shooter.Business.Objects
{
    public abstract class GameObject
    {

        public double Width { get; set; }

        public double Height { get; set; }

        public double Left { get; set; }

        public double Top { get; set; }

        public bool IsExpired 
        {
            get; set;
        }

        protected Rectangle Bound => new Rectangle
        {
            Left = Left,
            Top = Top,
            Width = Width,
            Height = Height
        };


        public abstract void Redraw(IGraphicsEngine graphicsEngine, IRenderRequest drawingContext);

        /// <summary>
        /// If game timer tick, every game object should update internall status
        /// </summary>
        public virtual void GameTimer(IGravityEngine gravityEngine, IAtmosfere atmosfere)
        {
            
        }

        public virtual bool CollidesWith(GameObject gameObject)
        {
            return Bound.Overlap(gameObject.Bound);
        }
    }
}