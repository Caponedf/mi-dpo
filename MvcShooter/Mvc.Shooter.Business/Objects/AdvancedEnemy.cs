using System;
using Cz.Cvut.Mvc.Shooter.Business.Atmosfere;
using Cz.Cvut.Mvc.Shooter.Business.Gravity;

namespace Cz.Cvut.Mvc.Shooter.Business.Objects
{
    public class AdvancedEnemy : Enemy
    {
        private readonly double screenWidth;
        private readonly double screenHeight;

        private int repositionCounter;
        private int repositionInterval;

        public AdvancedEnemy(Random random, double screenWidth, double screenHeight) : base(random)
        {
            this.screenWidth = screenWidth;
            this.screenHeight = screenHeight;
            repositionInterval = random.Next(1000);
        }

        public override void GameTimer(IGravityEngine gravityEngine, IAtmosfere atmosfere)
        {
            base.GameTimer(gravityEngine, atmosfere);

            if (repositionCounter >= repositionInterval)
            {
                repositionInterval = random.Next(1000);
                repositionCounter = 0;

                Left = random.Next((int)screenWidth);
                Top = random.Next((int)screenHeight);
            }
            repositionCounter++;
        }


    }
}