﻿using System;
using System.Collections.Generic;
using Cz.Cvut.Mvc.Shooter.Business.Graphics;

namespace Cz.Cvut.Mvc.Shooter.Business.Objects
{
    public enum CannonMode
    {
        SingleShooting,
        DoubleShooting
    }

    /// 
	/// <summary>
	/// @author Ondrej Stuchlik
	/// </summary>
	public class Cannon : GameObject
    {
        private AccDirection forceModifer = AccDirection.Increase;
        private readonly object mutex = new object();
        
        /// <summary>
        /// If true, cannon is armed by spacebar and as soos as user release spacebar, 
        /// missile is lunched.
        /// </summary>
        public bool IsReadyToFire { get; set; }

        public double Angle { get; set; }

        public double Force { get; set; }

        public CannonMode CannonMode { get; set; }

        public Cannon(double screenHeight)
        {
            Force = 10;

            Width = 25;
            Height = 69;

            Top = screenHeight / 2 - (Height / 2);

        }

        public override void Redraw(IGraphicsEngine graphicsEngine, IRenderRequest drawingContext)
        {
            graphicsEngine.DrawCannon(drawingContext, this);
        }

        public void AdjustForce(bool value)
        {
            IsReadyToFire = value;

            if (!value)
            {
                Force = 15;
                forceModifer = AccDirection.Increase;
                return;
            }

            lock (mutex)
            {
                var forceAcc = 1 * (int)forceModifer;

                Force += forceAcc;

                if (Force >= 99)
                    forceModifer = AccDirection.Decrease;
                if (Force <= 1)
                    forceModifer = AccDirection.Increase;

                if (Force < 0)
                    Force = 0;
            }
        }

        public void AdjustAngle(AccDirection direction)
        {
            Angle -= (int) direction;
        }

        public List<Missile> Fire()
        {
            var left = Width + 10;
            var top = Top + Height/2;
            switch (CannonMode)
            {
                case CannonMode.SingleShooting:
                    return new List<Missile> { new Missile(Angle, Force, left, top )};
                case CannonMode.DoubleShooting:
                    return new List<Missile>
                    {
                        new Missile(Angle - 5, Force, left, top),
                        new Missile(Angle + 5, Force, left, top)
                    };
                default:
                    throw new NotSupportedException(CannonMode + " is not supported.");

            }
        }

        
    }
}