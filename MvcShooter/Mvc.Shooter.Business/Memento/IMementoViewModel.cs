using Cz.Cvut.Mvc.Shooter.Business.ViewModel;

namespace Cz.Cvut.Mvc.Shooter.Business.Memento
{
    public interface IMementoViewModel
    {
        IMemento CreateMemento();

        void SetMemento(IMemento memento);
    }
}