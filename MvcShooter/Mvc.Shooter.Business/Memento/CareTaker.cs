using System.Collections.Generic;
using System.Linq;
using Prism.Commands;

namespace Cz.Cvut.Mvc.Shooter.Business.Memento
{
    public class CareTaker : ICareTaker
    {
        private const int UndoLimit = 5;

        private readonly IMementoViewModel operatorReference;
        private List<IMemento> mementos = new List<IMemento>();

        private int currentIndex = -1;

        /// <summary>
        /// If true, last invoker of <see cref="StoreMemento"/> was operator, 
        /// not <see cref="ICareTaker"/> by Undo or Redo.
        /// </summary>
        private bool isActualMemento;

        public bool CanUndo => currentIndex > 0;
        public bool CanRedo => currentIndex < mementos.Count - 1;

        public DelegateCommand UndoCommand { get; }
        public DelegateCommand RedoCommand { get; }


        public void StoreMemento()
        {
            isActualMemento = true;

            StoreMementoInternall(false);

            if (mementos.Count == UndoLimit)
            {
                mementos = mementos.Skip(1).ToList();
                currentIndex--;
            }
        }

        

        public CareTaker(IMementoViewModel operatorReference)
        {
            this.operatorReference = operatorReference;

            UndoCommand = new DelegateCommand(Undo, () => CanUndo);
            RedoCommand = new DelegateCommand(Redo, () => CanRedo);

            StoreMemento();
        }

        private void StoreMementoInternall(bool isUndoOperation)
        {
            if (!isUndoOperation)
                mementos = mementos.Take(currentIndex + 1).ToList();
            mementos.Add(operatorReference.CreateMemento());
            currentIndex++;

            UndoCommand.RaiseCanExecuteChanged();
            RedoCommand.RaiseCanExecuteChanged();
        }

        private void Undo()
        {
            if (isActualMemento)
                StoreMementoInternall(true);
            currentIndex--;
            operatorReference.SetMemento(mementos[currentIndex]);

            UndoCommand.RaiseCanExecuteChanged();
            RedoCommand.RaiseCanExecuteChanged();

            isActualMemento = false;

        }

        private void Redo()
        {
            currentIndex++;
            operatorReference.SetMemento(mementos[currentIndex]);
            RedoCommand.RaiseCanExecuteChanged();
            UndoCommand.RaiseCanExecuteChanged();
        }
    }
}