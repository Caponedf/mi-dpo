using Prism.Commands;

namespace Cz.Cvut.Mvc.Shooter.Business.Memento
{
    public interface ICareTaker
    {
        bool CanUndo { get; }

        bool CanRedo { get; }

        DelegateCommand UndoCommand { get; }

        DelegateCommand RedoCommand { get; }

      
        void StoreMemento();

    }
}