namespace Cz.Cvut.Mvc.Shooter.Business
{
    public class Rectangle
    {
        public double Left { get; set; }

        public double Top { get; set; }

        public double Width { get; set; }

        public double Height { get; set; }

        private bool ValueInRange(double value, double min, double max)
        {
            return (value >= min) && (value <= max);
        }

        public bool Overlap(Rectangle rect)
        {
            var xOverlap = ValueInRange(rect.Left, Left, Left + Width) ||
                           ValueInRange(Left, rect.Left, rect.Left + rect.Width);

            var yOverlap = ValueInRange(rect.Top, Top, Top + Height) ||
                           ValueInRange(Top, rect.Top, rect.Top + rect.Height);

            return xOverlap && yOverlap;
        }

        
    }
}