using System;
using Cz.Cvut.Mvc.Shooter.Business.Objects;

namespace Cz.Cvut.Mvc.Shooter.Business.GameLevels
{
    public class HardLevel : GameLevelBase
    {
        public override string Description => "Hard";
        public override int Missiles => 5;
        public override int Enemies => 5;

        protected override Enemy CreateNewEnemy(Random random, double screenWidth, double screenHeight)
        {
            return new AdvancedEnemy(random, screenWidth, screenHeight);
        }
    }
}