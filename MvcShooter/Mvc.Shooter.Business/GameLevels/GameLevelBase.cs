using System;
using Cz.Cvut.Mvc.Shooter.Business.Objects;
using Cz.Cvut.Mvc.Shooter.Business.ViewModel;

namespace Cz.Cvut.Mvc.Shooter.Business.GameLevels
{
    public abstract class GameLevelBase : IGameLevel
    {
        public abstract string Description { get; }
        public abstract int Missiles { get; }
        public abstract int Enemies { get; }

        public void NewGame(MainViewModel viewModel)
        {
            var random = new Random();

            for (var i = 0; i < viewModel.SelectedGameLevel.Enemies; i++)
            {
                var enemy = CreateNewEnemy(random, viewModel.ScreenWidth, viewModel.ScreenHeight);
                enemy.Left = random.Next((int)(viewModel.ScreenWidth - 50));
                enemy.Top = random.Next((int) (viewModel.ScreenHeight - 50));
                viewModel.Enemies.Add(enemy);
            }
        }

        protected virtual Enemy CreateNewEnemy(Random random, double screenWidth, double screenHeight)
        {
            return new Enemy(random);
        }
    }
}