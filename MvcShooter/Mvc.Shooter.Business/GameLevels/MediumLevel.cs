namespace Cz.Cvut.Mvc.Shooter.Business.GameLevels
{
    public class MediumLevel : EasyLevel
    {
        public override string Description => "Medium";
        public override int Missiles => 5;
        public override int Enemies => 4;
    }
}