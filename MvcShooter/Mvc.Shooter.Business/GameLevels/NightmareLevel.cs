namespace Cz.Cvut.Mvc.Shooter.Business.GameLevels
{
    public class NightmareLevel : HardLevel
    {
        public override string Description => "Nightmare";
        public override int Missiles => 3;
        public override int Enemies => 5;
    }
}