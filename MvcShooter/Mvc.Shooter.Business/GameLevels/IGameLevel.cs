﻿using Cz.Cvut.Mvc.Shooter.Business.ViewModel;

namespace Cz.Cvut.Mvc.Shooter.Business.GameLevels
{
    public interface IGameLevel
    {
        string Description { get; }

        int Missiles { get; }

        int Enemies { get; }

        void NewGame(MainViewModel viewModel);
    }
}
