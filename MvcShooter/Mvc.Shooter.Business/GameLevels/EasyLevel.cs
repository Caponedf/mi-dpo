namespace Cz.Cvut.Mvc.Shooter.Business.GameLevels
{
    public class EasyLevel : GameLevelBase
    {
        public override string Description => "Easy";
        public override int Missiles =>6;
        public override int Enemies => 3;
    }
}