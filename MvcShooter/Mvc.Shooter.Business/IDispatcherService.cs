﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cz.Cvut.Mvc.Shooter.Business
{
    public interface IDispatcherService
    {
        Task BeginInvoke(Action action);
    }
}
