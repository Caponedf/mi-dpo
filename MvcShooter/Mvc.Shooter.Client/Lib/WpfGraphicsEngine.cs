﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Cz.Cvut.Mvc.Shooter.Business.Objects;

namespace Cz.Cvut.Mvc.Shooter.Client.Lib
{
    /// 
    /// <summary>
    /// @author Ondrej Stuchlik
    /// </summary>
    public class WpfGraphicsEngine : GraphicsEngineBase<WpfRenderRequest>
    {
        private readonly BitmapImage cannonImage;
        private readonly BitmapImage enemyImage1;
        private readonly BitmapImage enemyImage2;
        private readonly BitmapImage missileImage;
        private readonly BitmapImage collisionImage;


        public WpfGraphicsEngine()
        {
            cannonImage = new BitmapImage(new Uri("Images/cannon.png", UriKind.Relative));
            enemyImage1 = new BitmapImage(new Uri("Images/enemy1.png", UriKind.Relative));
            enemyImage2 = new BitmapImage(new Uri("Images/enemy2.png", UriKind.Relative));
            missileImage = new BitmapImage(new Uri("Images/missile.png", UriKind.Relative));
            collisionImage = new BitmapImage(new Uri("Images/collision.png", UriKind.Relative));

        }

        protected override void DrawCannonInternal(WpfRenderRequest request, Cannon cannon)
        {
            DrawObject(request, cannon, cannonImage);

            var radians = (Math.PI/180)*cannon.Angle;
            var cannonAimWidth = Math.Cos(radians);
            var cannonAimTop = Math.Sin(radians);
            var force = cannon.Force;

            var pen = new Pen(new SolidColorBrush(Colors.Red), cannon.CannonMode == CannonMode.SingleShooting ? 1 :3);

            request.DrawingContext.DrawLine(pen,
                new Point(cannonImage.Width, cannon.Top+cannon.Height/2),
                new Point(cannonImage.Width + cannonAimWidth*force, cannon.Top + cannon.Height / 2 + cannonAimTop*force));
        }

        /// <summary>
        /// Draw base game object
        /// </summary>
        /// <param name="request"></param>
        /// <param name="gameObject"></param>
        /// <param name="bitmapImage"></param>
        private void DrawObject(WpfRenderRequest request, GameObject gameObject, BitmapImage bitmapImage)
        {
            var rect = new Rect(gameObject.Left, gameObject.Top, gameObject.Width,
                gameObject.Height);
            request.DrawingContext.DrawImage(bitmapImage, rect, null);
        }

        protected override void DrawMissileInternal(WpfRenderRequest request, Missile missile)
        {
            DrawObject(request, missile, missileImage);
        }

        protected override void DrawEnemyInternal(WpfRenderRequest request, Enemy enemy)
        {
            DrawObject(request, enemy, enemy is AdvancedEnemy ? enemyImage2 : enemyImage1);
        }

        protected override void DrawExplosionInternal(WpfRenderRequest request, Explosion explosion)
        {
            DrawObject(request, explosion, collisionImage);

        }
    }
}