﻿using Cz.Cvut.Mvc.Shooter.Business.Graphics;
using Cz.Cvut.Mvc.Shooter.Business.Objects;

namespace Cz.Cvut.Mvc.Shooter.Client.Lib
{
    public abstract class GraphicsEngineBase<TDrawingContext> : IGraphicsEngine
    {
        public void DrawCannon(object drawingContext, Cannon cannon)
        {
            DrawCannonInternal((TDrawingContext) drawingContext, cannon);
        }

        public void DrawMissile(object drawingContext, Missile missile)
        {
            DrawMissileInternal((TDrawingContext) drawingContext, missile);
        }

        public void DrawEnemy(object drawingContext, Enemy enemy)
        {
            DrawEnemyInternal((TDrawingContext)drawingContext, enemy);
        }

        public void DrawExplosion(object drawingContext, Explosion explosion)
        {
            DrawExplosionInternal((TDrawingContext)drawingContext, explosion);

        }

        protected abstract void DrawCannonInternal(TDrawingContext request, Cannon cannon);

        protected abstract void DrawMissileInternal(TDrawingContext request, Missile missile);

        protected abstract void DrawEnemyInternal(TDrawingContext request, Enemy missile);

        protected abstract void DrawExplosionInternal(TDrawingContext request, Explosion explosion);


    }
}