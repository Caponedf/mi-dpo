﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Cz.Cvut.Mvc.Shooter.Business.Graphics;

namespace Cz.Cvut.Mvc.Shooter.Client.Lib
{
    public class WpfRenderRequest : IRenderRequest
    {
        public DrawingContext DrawingContext { get; }
        public double ScreenWidth { get; }
        public double ScreenHeight { get;  }

        public WpfRenderRequest(DrawingContext drawingContext, double screenWidth, double screenHeight)
        {
            DrawingContext = drawingContext;
            ScreenWidth = screenWidth;
            ScreenHeight = screenHeight;
        }
    }

}
