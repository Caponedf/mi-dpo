﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using Cz.Cvut.Mvc.Shooter.Business;

namespace Cz.Cvut.Mvc.Shooter.Client
{
    public class WpfDispatcherService : IDispatcherService
    {
        private readonly Dispatcher dispatcher;

        public WpfDispatcherService(Dispatcher dispatcher)
        {
            this.dispatcher = dispatcher;
        }

        public Task BeginInvoke(Action action)
        {
            return dispatcher.BeginInvoke(action).Task;
        }

    }
}
