﻿using System.Windows;
using System.Windows.Input;
using Cz.Cvut.Mvc.Shooter.Business.ViewModel;

namespace Cz.Cvut.Mvc.Shooter.Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainViewModel ViewModel { get; private set; }

        public MainWindow()
        {
            InitializeComponent();

            ViewModel = new MainViewModel(new WpfDispatcherService(Dispatcher));
            DataContext = ViewModel;
            KeyDown += GameCanvasKeyDown;
            KeyUp += GameCanvasKeyUp;
        }
        private void GameCanvasKeyUp(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Up:
                    ViewModel.KeyPadViewModel.IsUpArrowPressed = false;
                    break;
                case Key.Down:
                    ViewModel.KeyPadViewModel.IsDownArrowPressed = false;
                    break;
                case Key.Space:
                    ViewModel.KeyPadViewModel.IsSpacebarPressed = false;
                    break;
                
            }
        }

        private void GameCanvasKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Up:
                    ViewModel.KeyPadViewModel.IsUpArrowPressed = true;
                    break;
                case Key.Down:
                    ViewModel.KeyPadViewModel.IsDownArrowPressed = true;
                    break;
                case Key.Space:
                    ViewModel.KeyPadViewModel.IsSpacebarPressed = true;
                    break;
                case Key.LeftShift:
                    ViewModel.SwitchCannonMode();
                    break;
            }
        }

        private void CanvasOnLoaded(object sender, RoutedEventArgs e)
        {
            ViewModel.OnViewLoaded();
        }
    }
}
