﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Cz.Cvut.Mvc.Shooter.Business.Graphics;
using Cz.Cvut.Mvc.Shooter.Business.ViewModel;
using Cz.Cvut.Mvc.Shooter.Client.Lib;

namespace Cz.Cvut.Mvc.Shooter.Client.Controls
{
    public class GameCanvas : Canvas
    {
        private readonly IGraphicsEngine graphicsEngine = new WpfGraphicsEngine();

        public MainViewModel ViewModel => DataContext as MainViewModel;

        public GameCanvas()
        {
            if(!DesignerProperties.GetIsInDesignMode(this))
                Loaded += GameCanvasLoaded;
        }

     

        private void GameCanvasLoaded(object sender, RoutedEventArgs e)
        {
            ViewModel.InvalidateRequested += ViewModelInvalidateRequested;
        }

        private void ViewModelInvalidateRequested(object sender, EventArgs e)
        {
            Dispatcher.BeginInvoke(new Action(InvalidateVisual));

        }

        protected override void OnRender(DrawingContext dc)
        {
            base.OnRender(dc);

            if (ViewModel != null)
            {
                lock (ViewModel.Mutex)
                {
                    ViewModel.ScreenWidth = ActualWidth;
                    ViewModel.ScreenHeight = ActualHeight;

                    foreach(var gameObj in ViewModel.GameObjects)
                        gameObj.Redraw(graphicsEngine, new WpfRenderRequest(dc, ActualWidth, ActualHeight));
                }
            }
        }
    }
}
