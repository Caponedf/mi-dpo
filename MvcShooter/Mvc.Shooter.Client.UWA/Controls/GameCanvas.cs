﻿using System;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Cz.Cvut.Mvc.Shooter.Business.Graphics;
using Cz.Cvut.Mvc.Shooter.Business.ViewModel;

namespace Mvc.Shooter.Client.UWA.Controls
{
    public class GameCanvas : Canvas
    {
        //private IGraphicsEngine graphicsEngine = new WpfGraphicsEngine();

        public MainViewModel ViewModel => DataContext as MainViewModel;

        public GameCanvas()
        {
            Loaded += GameCanvasLoaded;  
            
        }

        private void GameCanvasLoaded(object sender, RoutedEventArgs e)
        {
            ViewModel.InvalidateRequested += ViewModelInvalidateRequested;
            
        }


        private void ViewModelInvalidateRequested(object sender, System.EventArgs e)
        {
            Dispatcher.RunAsync(CoreDispatcherPriority.Normal, InvalidateArrange);

            CanvasDevice device = CanvasDevice.GetSharedDevice();
            CanvasRenderTarget renderTarget = new CanvasRenderTarget(device, (int)inkCanvas.ActualWidth, (int)inkCanvas.ActualHeight, 96);

            using (var ds = renderTarget.CreateDrawingSession())
            {
                ds.Clear(Colors.White);
                ds.DrawInk(inkCanvas.InkPresenter.StrokeContainer.GetStrokes());
            }

        }

      
    }
}
