﻿using Cz.Cvut.Mvc.Shooter.Business.ViewModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Mvc.Shooter.Tests
{
    [TestClass]
    public class MementoTest : GameTest
    {
        [TestMethod]
        public void UndoTest()
        {
            GameVictoryTest();

            Assert.IsTrue(viewModel.GameStatus == GameStatus.Victory);

            viewModel.CareTaker.UndoCommand.Execute().Wait();
            Assert.IsTrue(viewModel.GameStatus == GameStatus.Running);




        }
    }
}
