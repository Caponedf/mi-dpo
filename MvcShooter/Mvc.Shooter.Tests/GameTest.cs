using System.Threading;
using Cz.Cvut.Mvc.Shooter.Business.ViewModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Mvc.Shooter.Tests
{
    [TestClass]
    public class GameTest : TestBase
    {
        [TestMethod]
        public void GameVictoryTest()
        {
            Assert.IsTrue(viewModel.GameStatus == GameStatus.Running);
            viewModel.KeyPadViewModel.IsSpacebarPressed = true;
            Thread.Sleep(500);
            viewModel.KeyPadViewModel.IsSpacebarPressed = false;
            Thread.Sleep(2000);
            Assert.IsTrue(viewModel.GameStatus == GameStatus.Victory);
        }

        [TestMethod]
        public void GameFailTest()
        {
            Assert.IsTrue(viewModel.GameStatus == GameStatus.Running);
            viewModel.KeyPadViewModel.IsDownArrowPressed = true;
            Thread.Sleep(500);
            viewModel.KeyPadViewModel.IsDownArrowPressed = false;

            viewModel.KeyPadViewModel.IsSpacebarPressed = true;
            Thread.Sleep(500);
            viewModel.KeyPadViewModel.IsSpacebarPressed = false;
            Thread.Sleep(2000);
            viewModel.Missiles[0].IsExpired = true;
            Thread.Sleep(100);
            Assert.IsTrue(viewModel.GameStatus == GameStatus.Fail);
        }

        
    }
}