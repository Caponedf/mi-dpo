﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Cz.Cvut.Mvc.Shooter.Business;
using Cz.Cvut.Mvc.Shooter.Business.Atmosfere;
using Cz.Cvut.Mvc.Shooter.Business.GameLevels;
using Cz.Cvut.Mvc.Shooter.Business.Gravity;
using Cz.Cvut.Mvc.Shooter.Business.Objects;
using Cz.Cvut.Mvc.Shooter.Business.ViewModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Mvc.Shooter.Tests
{
    [TestClass]
    public class TestBase
    {
        protected MockRepository mockRepository;
        protected MainViewModel viewModel;

        [TestInitialize]
        public void TestInitialize()
        {
            mockRepository = new MockRepository();

            InitMainViewModel();

            CreateTestGameEnviroment();
        }

        private void InitMainViewModel()
        {
            var dispatcherService = MockRepository.GenerateMock < IDispatcherService >();

            dispatcherService.Stub(x => x.BeginInvoke(Arg<Action>.Is.Anything)).Return(CreateEmptyCompletedTask())
                .WhenCalled(m =>
                             {
                                 ((Action)m.Arguments[0])();
                                 m.ReturnValue = CreateEmptyCompletedTask();
                             });

            viewModel = MockRepository.GenerateMock<MainViewModel>(dispatcherService);

            viewModel.ScreenWidth = 800;
            viewModel.ScreenHeight = 600;

            viewModel.OnViewLoaded();
        }

        private void CreateTestGameEnviroment()
        {
            var random = MockRepository.GenerateMock<Random>();

            random.Expect(x => x.Next(750)).Return(385);
            random.Expect(x => x.Next(550)).Return(300);
            random.Expect(x => x.Next(1000)).Return(50);

            var testGameLevel = MockRepository.GenerateStub<IGameLevel>();
            testGameLevel.Stub(x => x.Missiles).Return(1);

            testGameLevel.Expect(x => x.NewGame(viewModel)).WhenCalled(vm => CreateEnemies(random));


            viewModel.SelectedGameLevel = testGameLevel;


            Assert.IsTrue(viewModel.GameStatus == GameStatus.NoGame);

            viewModel.NewGameCommand.Execute(null);
            viewModel.SelectedAtmosfere = viewModel.Atmosferes.FirstOrDefault(f => f is Vakuum);
            viewModel.SelectedGravityEngine = viewModel.GravityEngines.FirstOrDefault(f => f is NoGravityEngine);
        }

        protected virtual void CreateEnemies(Random random)
        {
            var enemy = new Enemy(random)
            {
                Left = random.Next((int) (viewModel.ScreenWidth - 50)),
                Top = random.Next((int) (viewModel.ScreenHeight - 50))
            };
            viewModel.Enemies.Add(enemy);
        }

        private Task CreateEmptyCompletedTask()
        {
            var taskSource = new TaskCompletionSource<object>();

            taskSource.SetResult(null);

            return taskSource.Task;
        }

       
    }

}
