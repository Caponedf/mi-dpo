﻿using System.Linq;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Mvc.Shooter.Tests
{
    [TestClass]
    public class EnemyTest : TestBase
    {
        [TestMethod]
        public void TestEnemy()
        {
            Assert.IsTrue(viewModel.Enemies.Any());

            var enemy = viewModel.Enemies[0];
            var left = enemy.Left;
            var top = enemy.Top;

            Thread.Sleep(500);

            Assert.AreEqual(left, enemy.Left);
            Assert.AreEqual(top, enemy.Top);
        }
    }
}
