using System;
using System.Linq;
using System.Threading;
using Cz.Cvut.Mvc.Shooter.Business.Objects;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Mvc.Shooter.Tests
{
    [TestClass]
    public class AdvancedEnemyTest : TestBase
    {
        [TestMethod]
        public void TestEnemy()
        {
            Assert.IsTrue(viewModel.Enemies.Any());

            var enemy = viewModel.Enemies[0];
            var left = enemy.Left;
            var top = enemy.Top;

            Thread.Sleep(1000);

            Assert.AreNotEqual(left, enemy.Left);
            Assert.AreNotEqual(top, enemy.Top);
        }

        protected override void CreateEnemies(Random random)
        {
            var enemy = new AdvancedEnemy(random,800,600)
            {
                Left = random.Next((int)(viewModel.ScreenWidth - 50)),
                Top = random.Next((int)(viewModel.ScreenHeight - 50))
            };
            viewModel.Enemies.Add(enemy);
        }
    }
}