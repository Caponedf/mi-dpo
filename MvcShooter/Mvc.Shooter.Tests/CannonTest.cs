﻿using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Mvc.Shooter.Tests
{
    [TestClass]
    public class CannonTest : TestBase
    {
        [TestMethod]
        public void TestCannonAngle()
        {
            var startAngle = viewModel.Cannon.Angle;

            viewModel.KeyPadViewModel.IsUpArrowPressed = true;
            Thread.Sleep(500);
            viewModel.KeyPadViewModel.IsUpArrowPressed = false;
            Assert.IsTrue(startAngle > viewModel.Cannon.Angle);

            viewModel.KeyPadViewModel.IsDownArrowPressed = true;
            Thread.Sleep(1000);
            Assert.IsTrue(startAngle < viewModel.Cannon.Angle);
        }

        [TestMethod]
        public void TestCannonForce()
        {
            var startForce = viewModel.Cannon.Force;

            viewModel.KeyPadViewModel.IsSpacebarPressed = true;
            Thread.Sleep(500);
            Assert.IsTrue(startForce < viewModel.Cannon.Force);
        }

    }
}
