package cz.fit.dpo.cbi.arithmetic.iterator;

import java.util.Iterator;

import cz.fit.dpo.cbi.arithmetic.elements.ExpressionElement;

public class InOrderIterator implements Iterator<ExpressionElement> {

    @Override
    public boolean hasNext() {
        throw new UnsupportedOperationException("Don't know how to do it :(");
    }

    @Override
    public ExpressionElement next() {
        throw new UnsupportedOperationException("Don't know how to do it :(");
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Don't know how to do it :(");
    }
}
